<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/testsuite">
		<html>
			<head>
				<style>
					ul {
						margin-top: 10px;
						margin-bottom: 10px;
					}
					h2, h3 {
						color: #196619;
					}
				</style>
			</head>
			<body>
				<h1>TK-testsvit för <xsl:value-of select="id"/></h1>
				
				<h2>Om testsviten</h2>
				<p>Detta dokument beskriver testsviten för <xsl:value-of select="id"/>. Testsviten innehåller testfall som kan användas för att verifiera implementationen innan integrationen med den nationella tjänsteplattformen.<br/>
				Testsviten använder SoapUI för att verifiera implementationen. Dokumentation om SoapUI hittas här: <a target="blank" href="https://www.soapui.org/getting-started/introduction.html">www.soapui.org</a>.<br/>
				Klicka på <a target="blank" href="https://www.soapui.org/downloads/soapui.html">den här länken</a> för att ladda hem en gratisversion av SoapUI. Installera enligt anvisning.</p>
				
				<h2>Förberedelser</h2>
				<p>
					<ul>
						<li>Gå till mappen <b>test-suite</b> i release-paketet</li>
						<li>Kopiera filen <b>‘soapui-support-N.N.NN.jar’</b> ('N.N.NN' är versionsnummer) till mappen <b>/bin/ext</b> där Soap-UI är installerat (leta efter mappen <b>/Program Files/Smartbear</b> på PC eller <b>/contents/java/app</b> på MAC OS)</li>
						<li>Öppna SoapUI och importera SoapUI-projektet (<b>test-suite/<xsl:value-of select="contractName"/>/<xsl:value-of select="contractName"/>-soapui-project.xml</b>) (välj ‘Import Project’ från menyn 'File')</li>
						<li>Om din webservice kräver ett SSL-certifikat, kan detta konfigureras via 'Preferences' (via menyn 'File').  
						I fönstret för inställningar, gå till fliken 'SSL Settings' och importera ditt certifikat under 'Keystore'</li>
						<li>Uppdatera <i>data.xml</i> så att den matchar den testdata du har i ditt system. Om du inte har testdata som passar så behöver detta läggas in i källsystemet (se nedan för instruktioner)</li>
						<li>Du borde nu kunna köra testfallen i SoapUI</li>
					</ul>
				</p>
				
				<h2>Testdata i <i>data.xml</i></h2>
				<p>
					Innan man kör testfallen i SoapUI så måste den data som skickas med i anropen anpassas utifrån det system som man vill testa. Detta görs genom att ändra i filen <i>data.xml</i> enligt nedan.<br/>
					<br/>
					Filen är i XML-format och i början finns en sektion som heter "globaldata". Här anger man den konfiguration som kommer att användas av alla testfall.<br/>
					Varje element i "globaldata" kan omdefinieras för ett specifikt testfall vid behov. Följande element är globala:
					<ul>
						<xsl:for-each select="globaldata/*">
							<li>
								<xsl:value-of select="name()"/>
							</li>
						</xsl:for-each>
					</ul>
				</p>
				
				<h2>Allmänna tips</h2>
				<p>
					<ul>
						<li>Kör ett testfall i taget och verifiera resultatet. Man kan också köra en hel testsvit (t.ex. "1 Basic tests") för att köra igenom alla testfall i just den sviten.<br/>
						Om du gör någon ändring som ska påverka ett specifikt testfall, kan man efter att ha verifierat just det testfallet köra genom hela sviten för att snabbt se att det "hänger ihop".</li>
						<li>Eventuella felmeddelanden skrivs ut dels till fönstret för testfallet och dels i sektionen "assertions" i anrops-fönstret.</li>
					</ul>
				</p>
				
				<h2>Domänens Mock-tjänster</h2>
				<p>
					Domänens mock-tjänster returnerar svarsmeddelanden från de filer som finns i katalogen <i>(/response)</i> under respektive mock-tjänst.<br/>
					Mock-tjänsterna använder <i>measureId</i> från request för att välja den fil som returneras. Filnamn anges utan filändelse.<br/>
					Värdet på <i>measureId</i> anges i xml-filen för testdata.<br/>
					<br/>
					
					Filen <i>Response-ChecksumOnly.xml</i> innehåller ett svar utan mätvärden.
					Innehållet i elementet <i>measurementChecksum</i> skall vara identiskt med motsvarande element i övriga response-filer.  
					
					<h3>Att använda mock-tjänsterna med egna xml-filer</h3>					
					Mock-tjänsterna kan användas med egna response-filer.<br/>
					För att mock-tjänsterna skall returnera önskat innehåll gör följande:
					<ul>
						<li>Skapa en fil t.ex. <i>(My-response.xml)</i> innehållande response och placera i katalogen <i>(/response)</i></li>
						<li>Se till att värdet i elementet <i>measurementChecksum</i> motsvarar värdet i filen <i>Response-ChecksumOnly.xml</i></li>
						<li>Uppdatera värdet på <i>measureId</i> i filen för testdata så att det motsvarar aktuellt filnamn (utan filändelse).</li>
					</ul>
					
				</p>
				
				
				
				<h2>Beskrivning av testfallen</h2>
				<p>De parametrar man anger för ett specifikt testfall kompletterar och/eller omdefinierar de parametrar som anges i "globaldata".<br/>
				Det betyder att både parametrar från "globaldata" och det specifika testfallets sektion i filen används för det aktuella testfallet.<br/>
				OBS! Om en parameter med samma namn definieras både i "globaldata" och specifikt för testfallet, så kommer värdet från testfalls-sektionen att användas.<br/>
				Glöm inte att spara <i>data.xml</i> efter att du har ändrat i den.</p>
				<ul style="list-style-type:none">
					<xsl:for-each select="testcase|section">
						<h3>
							<li>
								<xsl:value-of select="@id"/>
							</li>
						</h3>
						<p>
							<xsl:choose>
								<xsl:when test="starts-with(@id, '1.1.1 ')">Filtrering. Testfall för filtrering på personnummer. Verifierar att resultatet endast innehåller poster för given patient.</xsl:when>
								<xsl:when test="starts-with(@id, '1.1.2 ')">Filtrering. Testfall för filtrering på samordningsnummer. Ersätt patientId med det samordningsnummer som du vill filtrera på.</xsl:when>
								<xsl:when test="starts-with(@id, '1.1.3 ')">Filtrering. Testfall för filtrering på nationell reservidentitet. Ersätt personPatientId med den nationella reservidentitiet som du vill filtrera på.</xsl:when>
								<xsl:when test="starts-with(@id, '1.3 ')">Filtrering. Verifierar att resultatet endast innehåller poster för given vårdenhet.</xsl:when>
								<xsl:when test="starts-with(@id, '1.4 ')">Filtrering. Verifierar att resultatet endast innehåller poster för givet källsystem.</xsl:when>
								<xsl:when test="starts-with(@id, '1.5 ')">Filtrering. Verifierar att resultatet endast innehåller poster för given vårdkontakt.
									<br/><b>careContactId</b> är vårdkontaktens unika id.</xsl:when>
								<xsl:when test="starts-with(@id, '1.6 ')">Filtrering. Verifierar att resultatet endast innehåller poster för given vårdgivare.</xsl:when>
								<xsl:when test="starts-with(@id, '1.7 ')">Filtrering. Verifierar att tjänsteproducenten kan filtrera resultatet baserat på HTTP-headern <b>x-rivta-original-serviceconsumer-hsaid</b>. 
									<br/>Undersök och jämför det ofiltrerade svaret från producenten med det senare svaret vars begäran använt ett tjänstekonsumentHSAId på vilket producenten utfört bortfiltrering,<br/> 
									antalet poster skall i det senare svaret vara färre och parametern filterString skall då inte finnas i det filtrerade svaret. Testfallet är ej applicerbart för system som inte implementerat denna typ av filtrering.</xsl:when>
								<xsl:when test="starts-with(@id, '1.8 ')">Verifierar att resultatet är ett SOAP Exception. Detta testfall kräver att tjänsteproducenten skapar förutsättningar för ett internt fel att uppstå.<br/>
									Exempel kan vara att man stänger av kopplingen mot databas.</xsl:when>
								<xsl:when test="starts-with(@id, '2.1 ')">Verifierar att
									<ul>
										<li>Header-attributet "Content-type" har, om attributet finns, en teckenuppsättning som är satt till UTF-8 eller UTF-16</li>
										<li>Attributet "XML Prolog" har, om attributet finns, en teckenuppsättning som är satt till UTF-8 eller UTF-16</li>
										<li>Om båda attributen finns så ska de vara lika</li>
									</ul></xsl:when>
								<xsl:when test="starts-with(@id, '2.2 ')">Verifierar att responsen innehåller en sträng med specialtecken.<br/>
									Denna sträng behöver läggas upp på en post i källsystemet och bör innehålla så många specialtecken som möjligt.</xsl:when>
								<xsl:when test="starts-with(@id, '3.1 ')">Verifierar att alla returnerade poster innehåller elementen <b>accountableCareUnit</b> och <b>accountableCareGiver</b> i headern, som krävs för PDL-loggning.</xsl:when>
								<xsl:when test="starts-with(@id, '4.1 ')">Verifierar att tjänsteproducenten kan returnera en post som talar om att informationen får delas till patient. Element <b>approvedForPatient</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '4.2 ')">Verifierar att tjänsteproducenten kan returnera en post som talar om att informationen inte får delas till patient. Element <b>approvedForPatient</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '5.1 Signed')">Verifierar att tjänsteproducenten kan returnera en signerad post. Element <b>signature</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '5.2 Unsigned')">Verifierar att tjänsteproducenten kan returnera en osignerad post. Element <b>signature</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '5.3 Locked')">Verifierar att tjänsteproducenten kan returnera en post som har låsts av systemet efter att en viss tid har förflutit utan att någon har signerat den. Element <b>lockTime</b> i headern.</xsl:when>
								<xsl:when test="starts-with(@id, '6.1 Loadtest')"><b>6.1.1 Grund</b><br />
								Syftet med testet är dels att verifiera att systemet kan hantera minst 10 samtidiga trådar, dels att skapa sig en bild av systemets prestanda. Testet är designat att ta max 3 minuter.<br />
								I SLA-kapitlet i självdeklarationen, under "Övrig kommentar", ange genomsnittlig responstid. Ta värdet som visas i sista raden i kolumn "avg" och dela med antal anrop (oftast 2). Ange sedan värdet med enhet millisekunder (ms).

								<br /><br /><b>6.1.2 Uthållighet</b><br />
								Syftet med testet är att undersöka prestanda hos systemet över längre tid (30 minuter). I SLA-kapitlet i självdeklarationen, under "Övrig kommentar", notera om testet gick att genomföra utan problem. Om inte, notera hur lång tid det var möjligt att köra.	</xsl:when>
								<xsl:when test="starts-with(@id, '6.2 Recovery')"><b>6.2.1 Återhämtning</b><br />
								Syftet med testet är att utsätta systemet för maximal last och att verifiera att systemet automatiskt återhämtar sig. För att kontrollera att systemet har kunnat återhämta sig efter maximal last så rekommenderas att köra testfall "1.1.1 Personnummer" för att se att systemet svarar. I SLA-kapitlet i självdeklarationen, under "Övrig kommentar", notera om systemet kunde återhämta sig efter att ha utsatts för maximal last. Ange även hur många trådar som testet avslutades med.</xsl:when>
								<xsl:otherwise>
									<xsl:copy-of select="description"/>
								</xsl:otherwise>							
							</xsl:choose>						
						</p>
						<xsl:if test="data/*">
							<b>Testfalls-specifika parametrar</b>
						</xsl:if>
						<ul>
							<xsl:for-each select="data/*">
								<li>
									<xsl:value-of select="name()"/>
								</li>
							</xsl:for-each>
						</ul>
					</xsl:for-each>
				</ul>
				<br/>
				<br/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet> 

